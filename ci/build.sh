#!/usr/bin/env sh

set -e

if [[ -z "${CONTAINER_TEST_IMAGE}" ]]; then
  CONTAINER_TEST_IMAGE=test-image
fi

set -v

docker build -t ${CONTAINER_TEST_IMAGE} container
docker push ${CONTAINER_TEST_IMAGE}
