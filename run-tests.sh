#!/usr/bin/env sh

set -v
set -e

CONTAINER_NAME="$1"
CONTAINER_TEST_IMAGE="$2"

docker rm -f ${CONTAINER_NAME} 2>&1 > /dev/null || true

docker run --rm --name ${CONTAINER_NAME} \
  -p 2222:22 \
  -e SSH_USER_PUB_KEY="$( cat ${PWD}/ssh/id_rsa.pub )" \
  -v ${PWD}/ssh/id_rsa:/root/id_rsa \
  -d ${CONTAINER_TEST_IMAGE}

# tests
docker exec -t ${CONTAINER_NAME} ash -c 'ps aux' | grep "sshd"

docker exec -t ${CONTAINER_NAME} ash -c 'chmod 600 /root/id_rsa && ssh -i /root/id_rsa -o "StrictHostKeyChecking no" bender@localhost exit'

# clean up
docker rm -f ${CONTAINER_NAME}
