#!/usr/bin/env ash

ARG=${1}

if [[ "${ARG}" == "ash" ]]; then
  exec ${@}
else

  if [[ -z "${SSH_USER_PUB_KEY}" ]]; then
    echo "##> No point in starting sshd without installing a pub key"
    exit 1
  fi

  echo ${SSH_USER_PUB_KEY} >> /home/bender/.ssh/authorized_keys

  # Keep SSHD running after every disconnect
  while true; do
    /usr/sbin/sshd -Dd
  done
fi
