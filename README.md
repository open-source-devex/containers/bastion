# bastion

Docker container image for bastions

## Usage

Start the container
```bash
ci/build.sh
docker run -p 2222:22 -e SSH_USER_PUB_KEY='ssh-rsa AAAAB...MMkO9/+hdaqKNrZw==' test-image
```
